@extends('template')


@section('sousimage')
    @if(isset($users[1]))
        <button type="button" id="btn-clck1" class='btn btn-primary' style="width: 30%">Sélectionner étudiants</button>
        <br/>
        <button type="button" id="btn-clck2" class='btn btn-primary' style="width: 30%">Sélectionner membres BDE</button>
        <br/>
        <button type="button" id="btn-clck3" class='btn btn-primary' style="width: 30%">Sélectionner employés CESI</button>
        <br/>

        {{--<div class="panel panel-default">--}}
        {{--<div class="panel-body">--}}
        {{--<table class="table table-striped task-table">--}}

        {{--<!-- Table Headings -->--}}
        {{--<thead>--}}
        {{--<th>Task</th>--}}
        {{--<th>&nbsp;</th>--}}
        {{--</thead>--}}

        {{--<!-- Table Body -->--}}
        {{--<tbody>--}}
        <div id="easyPaginate">
            @include('listeUtilisateurs')
        </div>
        {{--</tbody>--}}
        {{--</table>--}}
        {{--</div>--}}
        {{--</div>--}}




        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="{{asset('js/jquery.easyPaginate.js')}}"></script>
        <script>
            $('#easyPaginate').easyPaginate({
                paginateElement: 'div',
                elementsPerPage: 5,
                effect: 'climb'
            });
        </script>
    @else
        @include('listeUtilisateurs')
    @endif
@endsection


@section('scripts')
    <script>
        $(document).ready(function(){
            function filter(e){
                $.ajax({
                    type:'POST',
                    url:'/utilisateurs?ajaxid=4',
                    data: {
                        filterParameter: e.data.filterParam
                    },
                    // cache: false,
                    {{--                    data: { _token: '{{csrf_token()}}' },--}}
                    success: function (data) {
                        $('.easyPaginateNav').remove();
                        $("#easyPaginate").html(data);
                        $('#easyPaginate').easyPaginate({
                            paginateElement: 'div',
                            elementsPerPage: 5,
                            effect: 'climb'
                        });
                    },
                    error: function (data, textStatus, errorThrown) {
                        console.log(e.data.filterParam);
                        console.log(data);
                        console.log(errorThrown);
                    },
                });
            }
            $("#btn-clck1").bind("click",{filterParam: "students"}, filter);
            $("#btn-clck2").bind("click",{filterParam: "members"}, filter);
            $("#btn-clck3").bind("click",{filterParam: "employees"}, filter);
            // $("#btn-clck1").click({filterParam: "students"}, filter);
            // $("#btn-clck2").click({filterParam: "members"}, filter);
            // $("#btn-clck3").click({filterParam: "employees"}, filter);
        });
    </script>
@endsection