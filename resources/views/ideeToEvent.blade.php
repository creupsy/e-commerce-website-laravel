@extends('template')
@section('head')
    <link rel="stylesheet" href="assets/css/contact.css">
@endsection
@section('titre')
    Ajout d'événement
@endsection

@section('sousimage')
    @foreach($idea as $idee)
    <div id="corps" class="panel-body">
        {!! Form::open(['url' => 'ideaToEvent', 'files' => true]) !!}
        <div class="form-group {!! $errors->has('titre') ? 'has-error' : '' !!}">
            {!! Form::text('titre', $idee['idea_name'], ['class' => 'form-control', 'placeholder' => 'Entrez une titre d\'événement']) !!}
            {!! $errors->first('titre', '<small class="help-block">:message</small>') !!}
        </div>

        <div class="form-group {!! $errors->has('date') ? 'has-error' : '' !!}">
            {!! Form::date('date', null, ['class' => 'form-control', 'placeholder' => 'Choisissez une date']) !!}
            {!! $errors->first('date', '<small class="help-block">:message</small>') !!}
        </div>
        <div class="form-group {!! $errors->has('price') ? 'has-error' : '' !!}">
            {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Entrez un prix']) !!}
            {!! $errors->first('price', '<small class="help-block">:message</small>') !!}
        </div>
        <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
            {!! Form::textarea ('description', $idee['idea_text'], ['class' => 'form-control', 'placeholder' => 'Entrez une description']) !!}
            {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
        </div>
        <div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">

            {!! Form::hidden ('image', null, ['class' => 'form-control', 'value' => $idee['idea_picture']]) !!}
            {!! Form::hidden ('id_idea', null, ['class' => 'form-control', 'value' => $idee['id_idea']]) !!}
            {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
        </div>

        {!! Form::submit('Créer l\'événement !', ['class' => 'btn btn-info pull-right']) !!}

        {!! Form::close() !!}

    </div>
    @endforeach
@endsection