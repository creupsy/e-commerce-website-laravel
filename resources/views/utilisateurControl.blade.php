@extends('template')

@section('titre')
    Accueil
@endsection
@section('head1')

    <link rel="stylesheet" href="assets/css/utilisateurControl.css" >

@endsection
@section('head')


@endsection

@section('contenu')

    <h2>Merci de vous connecter ou de vous inscrire pour acceder à cette page</h2>


@endsection

@section('sousimage')


    <h1> @foreach($users as $user)
             <div class="post-container">

                 <h3> <a href="{{ URL::action('PostutilisateurControlController@view', $user->id) }}">{{ $user->name }}</a></h3>
                 <p>{{ $user->email }}</p>
                 <p> Type d'utilisateur (1 : Admin, 2 : Etudiant, 3 : Personnel Cesi) : {{ $user->id_type_user }} </p>

             </div>

        @endforeach
    </h1>

    @guest

<div id="pouralign">
    <ul class="log">
        <li id="connexion"> <a href="/"> Connexion / Inscription </a> </li>
    </ul>


</div>
@endguest

@endsection