@extends('template')
@section('titre')
    boutique
@endsection
@section('head')
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/themes/smoothness/jquery-ui.css" />
    <!-- inclusion des libraries jQuery et jQuery UI (fichier principal et plugins) -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>


    <link rel="stylesheet" href="assets/css/boutique.css">

@endsection



@section('contenu')


    <div id="carouselHacked" class="carousel slide carousel-fade" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
        <?php $i=0;?>
                @foreach ($productOrder as $row)
            @if($i == 0)
                <div class="item active">

                <div class="carousel-caption">
                    <a href="details{{$row['id_product']}}">
                    <img id="imageproduit" src=<?php  echo($row['product_picture_url']); ?> ><img/>
                    </a>
                </div>
            </div>
                @endif
                    @if($i != 0)
                    <div class="item">
                        <div class="carousel-caption">
                            <a href="details{{$row['id_product']}}">
                            <img id="imageproduit" src=<?php  echo($row['product_picture_url']); ?> ><img/>
                                </a>
                        </div>
                    </div>
                        @endif
                    <?php $i++; ?>
            @endforeach




            </div>


        <!-- Controls -->
        <a class="left carousel-control" href="#carouselHacked" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carouselHacked" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>


    @endsection

@section('sousimage')
    <div>
        <button onClick="javascript:document.location.href='http://www.projetweb.exia/ajouterProduit'" type="button"  class="btn btn-success">ajouter produit</button>
    </div>

    <form id="barAndSearch" id="add" class="form-inline my-2 my-lg-0">

        <div class="dropdown">
            <button class="dropbtn">Trier</button>

            <form method="GET" action="">
            <div class="dropdown-content" onchange="javascript:submit(this)"   id="order" action="">
                <a href="#" data-url="http://www.projetweb.exia/boutique?order=asc">Ordre Croissant</a>
                <a href="#" data-url="http://www.projetweb.exia/boutique?order=desc">Ordre Décroissant </a>
            </div>
            </form>
        </div>
        <div class="barre">
            <div class="inner-addon right-addon enjoy-cssx">
                <i class="glyphicon glyphicon-search" ></i>
                <input id="search" type="search" placeholder="Search" aria-label="Search" placeholder="Search" class="form-control enjoy-css" />
            </div>
        </div>
    </form>








            <div class="conteneur">
                    @foreach($productOrder as $row)

                        <?php $details = "EUR  "?>

                        <div class="snip1423">
                            <div id="monimage">
                                <img id="imageproduit" src=<?php  echo($row['product_picture_url']); ?> ><img/>
                            </div>

                            <div class="intersnip">
                                <h3><?php  echo($row['product_name']);  ?></h3>
                                <p><?php  echo($row['product_description']); ?></p>
                                <div class="price">
                                    <span> <?php  echo $details.($row['product_price']); ?> </span>
                                </div>
                            </div><i class="ion-android-cart"></i><a href=<?php echo("'/details".$row['id_product']."'")?>></a>
                        </div>

                    @endforeach

            </div>


<script>
    //corespond en jquery a un document.ready
    document.addEventListener("DOMContentLoaded", function () {

        var options = document.querySelectorAll("#order a")

        for (var i = 0; i < options.length; i++) {
            options[i].onclick = function () {
                document.location.href = this.getAttribute("data-url");

            }

        }
    })

</script>
<script>

    var liste = [];
    @for($i = 0; $i < count($name); $i++)
    <?php echo("liste[".$i."] = '".$name[$i]."';"); ?>

    @endfor
    $('#search').autocomplete({
        source : liste
    });
    console.log(liste);
</script>

@endsection
