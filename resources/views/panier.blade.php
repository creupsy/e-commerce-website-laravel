@extends('template')

@section('head')
    <link rel="stylesheet" href="/assets/css/panier.css">
    @endsection



@section('contenu')
    <h1 id="titre">Voici votre panier {{$user['name']}}</h1>
    @endsection

@section('sousimage')


    <td id="trash" class="text-right"><button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> </button> </td>

    @foreach($order as $row)
        <div class="container mb-4">

            <?php $cart = App\Http\Controllers\commandeController::getCart($row['id_order']); ?>
            <div class="row">
                <div class="col-12">
                        @foreach($cart as $produit)
                            <?php $prod = App\Http\Controllers\commandeController::getProduct($produit['id_product']); ?>
                            @foreach($prod as $actualProd)
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col"> </th>
                                        <th scope="col">Produit</th>
                                        <th scope="col">Disponibilité</th>
                                        <th scope="col" class="text-center">Quantité</th>
                                        <th scope="col" class="text-right">Price</th>
                                        <th> </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                            <td id="img" style="max-width: 10px;"> <img class="img-fluid" src="{{$actualProd['product_picture_url']}}"></td>
                                        <td>{{$produit['amount']}}  {{$actualProd['product_name']}}</td>
                                        <td>In stock</td>
                                        <td><input class="form-control" type="text" value="{{$actualProd['product_count']}}" disabled="disabled" /></td>
                                        <td class="text-right">{{$actualProd['product_price']}}</td>

                                    </tr>

                                    </tbody>
                                </table>
                            @endforeach
                        @endforeach
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="row">
                        <div class="col-sm-12  col-md-6">
                            <button onClick="javascript:document.location.href='http://www.projetweb.exia/boutique'" class="btn btn-lg btn-block btn-danger">Continuer mes achats</button>
                        </div>
                        <div class="col-sm-12 col-md-6 text-right">
                            <button class="btn btn-lg btn-block btn-success">Payer</button>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    @endforeach

    <style>



    </style>
@endsection