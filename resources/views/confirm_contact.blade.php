@extends('template')
@section('head')
    <link rel="stylesheet" href="assets/css/contact.css">
@endsection
@section('contenu')
    <br>
    <div id="pageC" class="col-sm-offset-3 col-sm-6">
        <div id="infoC" class="panel panel-info">
            <div id="titreC" class="panel-heading">Contact</div>
            <div id="corpsC" class="panel-body">
                Merci. Votre message a été transmis à l'administrateur du site. Vous recevrez une réponse rapidement.
            </div>
        </div>
    </div>
@endsection