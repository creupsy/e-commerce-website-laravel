@extends('template')

@section('titre')
    Accueil
@endsection
@section('head1')

    <link rel="stylesheet" href="assets/css/utilisateurControl.css" >

@endsection
@section('head')


@endsection

@section('contenu')

    <h2>Merci de vous connecter ou de vous inscrire pour acceder à cette page</h2>


@endsection

@section('sousimage')

    <a href="{{ URL::action('utilisateurControlController@get') }}">Retour à la liste</a>
    <h1>{{ $user->name }}</h1>
    <p>{{ $user->email }}</p>
    <p> Type d'utilisateur (1 : Admin, 2 : Etudiant, 3 : Personnel Cesi) : {{ $user->id_type_user }} </p>
    <p>
        <a href="{{ URL::action('PostutilisateurControlController@edit', $user->id) }}" class="button">Editer</a>
    </p>

    @guest

<div id="pouralign">
    <ul class="log">
        <li id="connexion"> <a href="/"> Connexion / Inscription </a> </li>
    </ul>


</div>
@endguest

@endsection