
@extends('template')
<!--On "hérite" de la page template et on y ajoute du contenu -->
@section('titre')

    Contact
@endsection
@section('head')
    <!--lien vers la feuille css utilisée -->
    <link rel="stylesheet" href="assets/css/details.css">
@endsection

@section('sousimage')
    @if(isset($msg)) {{$msg}}@endif

    @foreach($product as $row)
<?php $euro=' EUR'?>
<script>
    function toto(id) {
            var NameDiv = document.getElementById("popup");
            var row = document.getElementById("row");
            var divco = document.getElementById("dynamique");
            console.log(NameDiv);
            console.log(divco);
            if (NameDiv.style.display = "none") {
                NameDiv.style.display = "block";
                divco.style.display = "none";
                row.style.display = "none";

            }


    }

</script>
    <div class="container">
        <div class="row">
            <div class="col-xs-4 item-photo">
                <img style="max-width:100%;" src="<?php echo $row['product_picture_url'] ?>" />
            </div>
            <div class="col-xs-5" style="border:0px solid gray">
                <!-- Datos del vendedor y titulo del producto -->
                <h3><?php echo $row['product_name'] ?></h3>


                <!-- Precios -->
                <h6 class="title-price"><small>Prix</small></h6>
                <h3 style="margin-top:0px;"><?php echo $row['product_price'].$euro ?></h3>

                <!-- Detalles especificos del producto -->
                <div class="col-xs-9">
                    <ul class="menu-items">
                        <li class="active">Description</li>

                    </ul>
                    <div style="width:100%;border-top:1px solid silver">
                        <p style="padding:15px;">
                            <small>
                                <?php echo $row['product_description'] ?>
                            </small>
                        </p>
                    </div>
                </div>

                {!! Form::open(['url' => 'addToCart']) !!}



                <input type="hidden" name='id_user' value=<?php echo("'".$user['id']."'")?> >
                <input type="hidden" name='id_product' value=<?php echo("'".$row['id_product']."'")?> >




                <div class="section" style="padding-bottom:20px;">

                    <div>
                        <p>Quantité :<br/></p>
                        <input name="amount" type="number" value="1" />

                    </div>
                </div>

                <!-- Botones de compra -->
                <div id="dynamique" class="section" style="padding-bottom:20px;">
                    {!! Form::submit('Ajouter au panier', null, ['id' => 'submit']) !!}

                   <!-- <button onclick="toto('popup')" class="btn btn-success"><span style="margin-right:20px" class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Agregar al carro</button> -->

                </div>

                {!! Form::close() !!}
            </div>


        </div>
        @endforeach
    </div>
<div id="popup">
    <div class="row">
<div class="col-lg-2">
    <button class="btn btn-info my-2 my-sm-0" type="submit">Search</button>
        </div>
        <div class="col-lg-8">
            <button class="btn btn-info my-2 my-sm-0" type="submit">Search</button>

        </div>
    </div>
</div>


@endsection