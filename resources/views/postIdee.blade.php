@extends('template')
@section('head')
    <link rel="stylesheet" href="assets/css/contact.css">
@endsection
@section('titre')
    Ajout d'événement
    @endsection

@section('sousimage')
    <div id="corps" class="panel-body">
        {!! Form::open(['url' => 'postIdee', 'files' => true]) !!}
        <div class="form-group {!! $errors->has('titre') ? 'has-error' : '' !!}">
            {!! Form::text('titre', null, ['class' => 'form-control', 'placeholder' => 'Titre de l\'événement']) !!}
            {!! $errors->first('titre', '<small class="help-block">:message</small>') !!}
        </div>
        <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
            {!! Form::textarea ('description', null, ['class' => 'form-control', 'placeholder' => 'Description de l\'événement']) !!}
            {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
        </div>
        <div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">
            <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
            {!! Form::file ('image', null, ['class' => 'form-control', 'placeholder' => 'Choisissez une image à importer']) !!}
            {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
        </div>


        <input type="hidden" name="user_id" value={{$user['id']}} />
        {!! Form::submit('Créer', ['class' => 'btn btn-info pull-right']) !!}

        {!! Form::close() !!}

    </div>
    @endsection