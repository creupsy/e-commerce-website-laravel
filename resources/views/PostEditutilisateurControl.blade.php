@extends('../template')

@section('titre')
    Accueil
@endsection
@section('head1')

    <link rel="stylesheet" href="../assets/css/utilisateurControl.css" >

@endsection
@section('head')


@endsection

@section('contenu')

    <h2>Merci de vous connecter ou de vous inscrire pour acceder à cette page</h2>


@endsection

@section('sousimage')

    <a href="{{ URL::action('utilisateurControlController@get') }}">Retour à la liste</a> - <a href="{{ URL::action('utilisateurControlController@get', $user->id) }}">Annuler</a>
    <h1>Editer un utilisateur</h1>

    <div id=users">
    {{--<tr>--}}
    <!-- Task Name -->
        {{--<td class="table-text">--}}
        <p><strong> Nom : {{ $user->name }} </br> Email : {{ $user->email }}</strong> (ID : {{ $user->id }})</p>
        {{--</td>--}}

        {{--<td>--}}
        {{--<section style="display: inline-block">--}}
        {!!  Form::open(['action' => ['UserController@update', $user->id], 'method' => 'POST']) !!}
        {{Form::label('id_type_user', 'Rang de l\'utilisateur :')}} {{Form::text('id_type_user', $user->id_type_user)}}
        {{Form::label('name', 'Nom d\'utilisateur')}} {{Form::text('name', $user->name)}}
        {{Form::label('email', 'Email :')}} {{Form::text('email', $user->email)}}
        {{Form::hidden('_method','PATCH')}}
        {!!Form::token()!!}
        {{Form::submit('Modifier',['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}

        {!!Form::open(['action'=> ['UserController@destroy', $user->id], 'method' => 'POST']) !!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Supprimer',['class'=>'btn btn-danger'])}}
        {!!Form::close() !!}
        {{--</section>--}}
        {{--<form action="/utilisateurs/{{ $user->id }}" method="POST">--}}
        {{--{{ csrf_field() }}--}}
        {{--{{ method_field('DELETE') }}--}}

        {{--<button type="submit" class="btn btn-danger">--}}
        {{--<i class="fa fa-btn fa-trash"></i>Delete--}}
        {{--</button>--}}
        {{--</form>--}}
        {{--</td>--}}
        {{--</tr>--}}
        <br/>
    </div>

    @guest

        <div id="pouralign">
            <ul class="log">
                <li id="connexion"> <a href="/"> Connexion / Inscription </a> </li>
            </ul>


        </div>
    @endguest

@endsection