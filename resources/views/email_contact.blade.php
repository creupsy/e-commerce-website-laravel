@extends('template')

@section('contenu')
<h1>Prise de contact depuis le site BDE</h1>
<p>Réception d'une prise de contact :</p>
<ul>
    <li><strong>Nom</strong> : {{ $nom }}</li>
    <li><strong>Email</strong> : {{ $email }}</li>
    <li><strong>Message</strong> : {{ $texte }}</li>
</ul>
@endsection