@extends('template')

@section('head')
    <link rel="stylesheet" href="/../../assets/css/styleEvent.css">
@endsection

@section('sousimage')

    <?php $i = 0; ?>

    <div class="allDivs">
        <div class="space"></div>
        <a href="/postEvent">Poster un événement</a>
        <div class="space"></div>

        @foreach($events as $row)

            <div class="DivEvent">

                <h1><?php echo $row['event_title']?></h1>
                <div class="description">

                    <a href=<?php echo("/eventPicture".$row['id_event']) ?>><?php echo("<img class=\"imgEvent\" src=".$row['event_picture_url']."//>"); ?></a>

                    <p>
                        <?php echo $row['event_text'] ?>
                    </p>
                    <br/><?php echo $row['event_date'] ?>
                </div>
            </div>

            <div class="space"></div>
        @endforeach
    </div>





 <!--   @foreach($events as $row)

        <div class="DivEvent">

            <h1><?php echo $row['event_title']?></h1>
            <div class="description">

                <a href=<?php echo("/eventPicture".$row['id_event']) ?>><?php echo("<img class=\"imgEvent\" src=".$row['event_picture_url']."//>"); ?></a>

                <p>
                    <?php echo $row['event_text'] ?>
                </p>
                <br/><?php echo $row['event_date'] ?>
            </div>
        </div>

        <div class="space"></div>
    @endforeach-->

@endsection