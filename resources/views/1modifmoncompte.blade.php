@extends('template')

@section('titre')
    Accueil
@endsection
@section('head1')


@endsection
@section('head')
    <link rel="stylesheet" href="assets/css/modifmoncompte.css">

@endsection

@section('contenu')

    <h3>
        Vos informations personnelles {{ $user["name"] }}
    </h3>

@endsection

@section('sousimage')

    <div class="moncompte">
        <!-- UI - X Starts -->
        <div class="ui-67">


            <!-- Content Starts -->
            <div class="ui-content">

                <!--<div class="row">

                    <div class="col-sm-8 col-md-8 col-lg-8 col-lg-offset-2 acc-col">
                        <section>
                            <h3>  </h3>
                            <form class="ng-pristine ng-valid" role="form">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="inputName" class="control-label">Prénom :</label>
                                        <input type="text" class="form-control" id="inputName">
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="col-sm-12">
                                            <a href="#" id="connexion">Update</a>
                                        </div>
                                    </div>



                                        <div class="col-sm-12">
                                            <label for="inputEmail3" class="control-label">Votre Email :</label>
                                            <input type="email" class="form-control" id="inputEmail3">
                                        </div>


                                    <div class="col-sm-12">
                                        <div class="col-sm-12">
                                            <a href="#" id="connexion">Update</a>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </section>


                      <section>
                            <h3 id="textlog" >Login Information</h3>

                                <div class="row">

                                    <div class="col-sm-12">
                                        <label for="inputEmail3" class="control-label">Votre nouveau mot de passe :</label>
                                        <div class="">
                                            <input type="password" class="form-control" id="inputEmail3">
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <label id="comfirm" for="inputPassword3" class="control-label">Confirmez votre nouveau mot de passe :</label>
                                        <div class="">
                                            <input type="password" class="form-control" id="inputPassword3">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-12">
                                            <a href="#" id="connexion">Update</a>
                                        </div>

                                    </div>

                                </div>

                        </section>-->

                <div>


                    <form class="section" action="/modification-prenom" method="post">
                        {{ csrf_field() }}

                        <div class="field">
                            <label class="label">Nouveau prénom</label>
                            <div class="control">
                                <input class="input" type="text" name="name">
                            </div>


                        <div class="field">
                            <div class="control">
                                <button class="button is-link" type="submit">Modifier mon prénom</button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
                <div>
                    <form class="section" action="/modification-email" method="post">
                        {{ csrf_field() }}

                        <div class="field">
                            <label class="label">Nouveau mail</label>
                            <div class="control">
                               <p>{{Form::label}}</p>
                            </div>


                            <div class="field">
                                <div class="control">
                                    <button class="button is-link" type="submit">Modifier mon email</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>


                <div>
                            <form class="section" action="/modification-mot-de-passe" method="post">
                                {{ csrf_field() }}

                                <div class="field">
                                    <label class="label">Nouveau mot de passe</label>
                                    <div class="control">
                                        <input class="input" type="password" name="password">
                                    </div>
                                    @if($errors->has('password'))
                                        <p class="help is-danger">{{ $errors->first('password') }}</p>
                                    @endif
                                </div>

                                <div class="field">
                                    <label class="label">Mot de passe (confirmation)</label>
                                    <div class="control">
                                        <input class="input" type="password" name="password_confirmation">
                                    </div>
                                    @if($errors->has('password_confirmation'))
                                        <p class="help is-danger">{{ $errors->first('password_confirmation') }}</p>
                                    @endif
                                </div>

                                <div class="field">
                                    <div class="control">
                                        <button class="button is-link" type="submit">Modifier mon mot de passe</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>

                    <!-- col-8 -->
                </div>

            </div>
            <!-- Content Ends -->
        </div>
        <!-- UI - X Ends -->
    </div>

@endsection