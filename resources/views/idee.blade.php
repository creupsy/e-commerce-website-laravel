@extends('template')

@section('head')
    <link rel="stylesheet" href="assets/css/styleEvent.css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>



@endsection

@section('sousimage')

    <?php $i = 0; ?>

    @auth
    <div class="allDivs">
        <div class="space"></div>
        <a href="/postIdee">Déposer une idée</a>
        <div class="space"></div>
        @endauth

        <div class="container">
            @foreach($ideas as $row)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href=<?php echo("/ideePicture".$row['id_idea']) ?> class="MakaleYazariAdi"><?php echo $row['idea_name']?> </a>

                        @if ( $user["id_type_user"] == "1")

                            <script type="text/javascript">

                                function toggle_div(id) {

                                    var div = document.getElementById(id);

                                    if (div.style.display == 'block') {

                                        div.style.display = 'none';

                                    } else {

                                        div.style.display = 'block';
                                    }
                                }

                            </script>

                            <div class="btn-group" style="float:right;">
                                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="glyphicon glyphicon-cog"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>

                                <ul class="dropdown-menu">
                                    <li><a href=<?php echo("'deleteIdea".$row['id_idea']."'") ?> ><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span> Delete</a></li>
                                </ul>

                            </div>

                        @endif

                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <a class="media-object" id="imgEvent" href=<?php echo("/ideaPicture".$row['id_idea']) ?>  > <?php echo("<img class=\"imgEvent\" src=".$row['idea_picture']."//>"); ?>
                                    </a>
                                </a>
                            </div>
                            <div class="media-right">
                                <?php echo($row['idea_text']) ?>
                            </div>
                        </div>

                    </div>

                </div>
                <p><?php $vote = App\Http\Controllers\voteController::getVotes($row['id_idea']); echo $vote;?> Votes</p>
                @auth

                    {!! Form::open(['url' => 'vote', 'files' => true]) !!}

                    <input type="hidden" name="id_idea" value=<?php echo("'".$row['id_idea']."'") ?>/>
                    {!! Form::submit('Voter', ['class' => 'btn btn-info pull-right']) !!}
                    {!! Form::close() !!}
                @endauth
                @if($user['id_type_user'] == 1)
                {!! Form::open(['url' => 'ideaToEventForm']) !!}

                <input type="hidden" name="id_idea" value=<?php echo("'".$row['id_idea']."'") ?>/>
                {!! Form::submit('Transformer en evenement', ['class' => 'btn btn-info pull-right']) !!}
                {!! Form::close() !!}
                @endif
            @endforeach
        </div>
    </div>

@endsection