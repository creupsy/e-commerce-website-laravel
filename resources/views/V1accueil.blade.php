@extends('template')

@section('titre')
    Accueil
    @endsection

@section('contenu')


    <script>  function ShowHide(id) {
            var NameDiv = document.getElementById("wrapper");
            var row = document.getElementById("row");
            var divco = document.getElementById("dynamique");
            console.log(NameDiv);
            console.log(divco);
            if (NameDiv.style.display = "none") {
                NameDiv.style.display = "block";
                divco.style.display = "none";
                row.style.display = "none";

            } else {
                NameDiv.style.display = 'none';
                divco.style.display = 'block';
            }
        };

    </script>
    <!-- Top content -->

        <div class="container center">
            <div class="row" id="row">
                <div class="col-sm-12 text wow fadeInLeft">
                    <h1>Votre Site Pour Votre Bde</h1>
                    <div class="description">
                        <p class="medium-paragraph">
                            Bienvenu sur le site officiel du bde de l'exia cesi
                        </p>
                    </div>
                </div>
            </div>

            <h1>
                {{ $message or '' }}
            </h1>

            <div id="dynamique">
                <ul class="log">
                        <li id="connexion" onclick="ShowHide('wrapper');"> Connexion / Inscription</li>
                </ul>
            </div>
            <div id="formlog">
                 <section>
                    <div id="container_demo" >
                        <a class="hiddenanchor" id="toregister"></a>
                        <a class="hiddenanchor" id="tologin"></a>
                        <div id="wrapper">
                            <div id="login" class="animate form">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <h1>Connexion</h1>
                                    <p>
                                        <label for="username" class="uname" data-icon="u" >{{ __('Votre adresse email :') }}</label>
                                        <input id="username" name="username" required="required" type="text" placeholder="mymail@mail.com"/>
                                    </p>
                                    <p>
                                        <label for="password" class="youpasswd" data-icon="p"> Votre mot de passe </label>
                                        <input id="password" name="password" required="required" type="password" placeholder="ex. X8df!90EO" />
                                    </p>
                                    <p class="login button">
                                        <input type="submit" value="Connexion" />
                                    </p>
                                    <p class="change_link">
                                        Vous n'avez pas de compte ?
                                        <a href="#toregister" class="to_register">Inscription</a>
                                    </p>
                                </form>
                            </div>


                            <div id="register" class="animate form">
                                <form  action="register.php" autocomplete="on" method="POST">
                                    <h1> Inscription </h1>
                                    <p>
                                        <label for="email" class="youmail" data-icon="e" > Votre email </label>
                                        <input id="email" name="email" required="required" type="email" placeholder="mysupermail@mail.com"/>
                                    </p>
                                    <p>
                                        <label for="password" class="youpasswd" data-icon="p"> Votre mot de passe </label>
                                        <input id="password" name="password" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                    </p>
                                    <p>
                                        <label for="password_confirm" class="youpasswd" data-icon="p"> Comfirmez votre mot de passe </label>
                                        <input id="password_confirm" name="password_confirm" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                    </p>
                                    <p class="signin button">
                                        <input type="submit" value="S'inscrire"/>
                                    </p>
                                    <p class="change_link">
                                        Vous avez déjà un compte ?
                                        <a href="#tologin" class="to_register">Connexion</a>
                                    </p>
                                </form>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
@endsection
@section('sousimage')


    <!-- Features -->
    <div class="features-container section-container">
        <div class="container">

            <div class="row">
                <div class="col-sm-12 features section-description wow fadeIn">
                    <h2>Voici Les Evenements Du Bde</h2>
                    <div class="divider-1"><div class="line"></div></div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 features-box wow fadeInLeft">
                    <div class="row">
                        <div class="col-sm-3 features-box-icon">
                            <i class="fa fa-twitter"></i>
                        </div>
                        <div class="col-sm-9">
                            <h3>Ut wisi enim ad minim</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.
                                Ut wisi enim ad minim veniam, quis nostrud.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 features-box wow fadeInLeft">
                    <div class="row">
                        <div class="col-sm-3 features-box-icon">
                            <i class="fa fa-instagram"></i>
                        </div>
                        <div class="col-sm-9">
                            <h3>Sed do eiusmod tempor</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.
                                Ut wisi enim ad minim veniam, quis nostrud.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 features-box wow fadeInLeft">
                    <div class="row">
                        <div class="col-sm-3 features-box-icon">
                            <i class="fa fa-magic"></i>
                        </div>
                        <div class="col-sm-9">
                            <h3>Quis nostrud exerci tat</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.
                                Ut wisi enim ad minim veniam, quis nostrud.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 features-box wow fadeInLeft">
                    <div class="row">
                        <div class="col-sm-3 features-box-icon">
                            <i class="fa fa-cloud"></i>
                        </div>
                        <div class="col-sm-9">
                            <h3>Minim veniam quis nostrud</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.
                                Ut wisi enim ad minim veniam, quis nostrud.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection