
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400|Roboto:300,400,500">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/style.css">
    @yield('head1')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link href='http://fonts.googleapis.com/css?family=Goudy+Bookletter+1911' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:100' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
    <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
    <meta name="author" content="Codrops" />
    <link rel="stylesheet" type="text/css" href="assets/css/animate-custom.css" />
    <title>@yield('titre')</title>
    @yield('head')

</head>
<body>
<!-- Top menu -->

<nav class="navbar navbar-inverse navbar-fixed-top navbar-no-bg" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="top-navbar-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/" style="padding: 0; padding-right: 20px;">
                        <img  id="logoo" src="assets/img/logo@2x.png" style="max-width: 50px;">
                    </a></li>
                <li><a href="/">Accueil</a></li>
                <li><a href="boutique">Boutique</a></li>
                <li><a href="events">Evenement</a></li>
                <li><a href="idee">Boite à idée</a></li>
                <li><a href="contact">Contact</a></li>
                <li><a href="moncompte">Mon Compte</a></li>

                <li><a class="btn btn-link-3" href="panier"><img id="panier" src="assets/ico/panier.png"/></a></li>


                @auth
                <li><a class="btn btn-link-3" href="{{ route('logout') }}"><img id="panier" src="assets/ico/logout.png" alt=""/></a></li>
                    @endauth
            </ul>
            </div>
        </div>
    </div>
    </nav>
    <div class="top-content">

        @yield('contenu')
    </div>

@yield('sousimage')


<!-- Footer -->
    <footer id="footer">
        <div class="container">
            <div class="row">


                <div class="col-sm-12 footer-copyright footer-one"><p>&copy; Cesi Corporation Exia www.projetweb.exia</p> <a href=#>Mentions Légales</a>
                    <h5>Rejoignez nous</h5>
                    <div class="social-icons">
                        <a href="https://www.facebook.com/"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                        <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                        <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                        <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </footer>
</div>



<!-- Javascript -->

<script src="assets/bootstrap/js/bootstrap.min.js"></script>

<script src="assets/js/wow.min.js"></script>
<script src="assets/js/retina-1.1.0.min.js"></script>
<script src="assets/js/waypoints.min.js"></script>
<script src="assets/js/scripts.js"></script>
<script src="assets/js/connexion.js"></script>

<!--[if lt IE 10]>
<script src="assets/js/placeholder.js"></script>
<![endif]-->

<![endif]-->

</body>
</html>