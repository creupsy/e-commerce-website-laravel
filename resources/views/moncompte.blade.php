@extends('template')

@section('titre')
    Accueil
@endsection
@section('head1')

@endsection
@section('head')
    <link rel="stylesheet" href="assets/css/modifmoncompte.css">

@endsection

@section('contenu')

    @auth
    <script>
        $(function () {
            $('a[title]').tooltip();
        });
    </script>


    <h3>
        Bienvenue sur votre compte {{ $user["name"] }}
    </h3>


    <div class="container">
        <ul class="nav nav-tabs1" id="myTab">

            <li><a href="moncompteParamètres"  title="profile">
                     <span class="round-tabs two">
                         <i class="glyphicon glyphicon-user"></i>
                     </span>
                </a>
            </li>

        </ul>
    </div>
    <div  id="param">
        <h3>Paramètres</h3>
    </div>
@endauth

    @guest

    <h2>Merci de vous connecter ou de vous inscrire pour acceder à cette page</h2>

    @endguest

@endsection

@section('sousimage')



    @auth
        <div class="container historique">
            <div class="row">
                <div class="col-md-12">
                    <div id="boite" class="content-box well">

                        <legend>Historique des commandes</legend>

                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Nom du produit</th>
                                        <th>Prix</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><a href="#">PHP BAPTOU</a></td>
                                        <td>PHP PRIX BAPTOUU</td>
                                        <td>PHP DATE BAPTOU</td>
                                        <td>PHP STATUS BAPTOU</td>
                                    </tr>
                                    <tr>
                                        <td><a href="#">PHP BAPTOU</a></td>
                                        <td>PHP PRIX BAPTOUU</td>
                                        <td>PHP DATE BAPTOU</td>
                                        <td>PHP STATUS BAPTOU</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endauth


    @guest

<div id="pouralign">
    <ul class="log">
        <li id="connexion"> <a href="/"> Connexion / Inscription </a> </li>
    </ul>


</div>




@endguest

@endsection