@extends('template')
@section('head')
    <link rel="stylesheet" href="assets/css/contact.css">
@endsection
@section('titre')
    Ajout d'événement
@endsection

@section('sousimage')
    <div id="corps" class="panel-body">
        {!! Form::open(['url' => 'ajouterProduit', 'files' => true]) !!}
        <div class="form-group {!! $errors->has('nom_produit') ? 'has-error' : '' !!}">
            {!! Form::text('nom_produit', null, ['class' => 'form-control', 'placeholder' => 'nom produit']) !!}
            {!! $errors->first('name', '<small class="help-block">:message</small>') !!}
        </div>

        <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
            {!! Form::textarea ('description', null, ['class' => 'form-control', 'placeholder' => 'Description du produit']) !!}
            {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
        </div>
        <div class="form-group {!! $errors->has('price') ? 'has-error' : '' !!}">
            {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Entrez un prix']) !!}
            {!! $errors->first('price', '<small class="help-block">:message</small>') !!}
        </div>

        <div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">
            <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
            {!! Form::file ('image', null, ['class' => 'form-control', 'placeholder' => 'Choisissez une image à importer']) !!}
            {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
        </div>
        <div class="form-group {!! $errors->has('type_produit') ? 'has-error' : '' !!}">
            {!! Form::text('type_produit', null, ['class' => 'form-control', 'placeholder' => 'vetements tappez:1 accessoires tappez:2 autre tappez:3']) !!}
            {!! $errors->first('type_produit', '<small class="help-block">:message</small>') !!}
        </div>



        {!! Form::submit('Ajouter le produit', ['class' => 'btn btn-info pull-right']) !!}

        {!! Form::close() !!}

    </div>
@endsection