@extends('template')

@section('head')
    <link rel="stylesheet" href="assets/css/styleEvent.css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>



@endsection

@section('sousimage')

    <?php $i = 0; ?>

    @auth
        @if ( $user["id_type_user"] == "1")
            <div class="allDivs">
                <div class="space"></div>
                <a href="/postEvent">Poster un événement</a>
                <div class="space"></div>
        @endif



        <div class="container">
            @foreach($events as $row)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a style="text-decoration: none;"><?php echo $row['event_title']?> </a>
                        <div class="btn-group" style="float:right;">

                                @if ( $user["id_type_user"] == "1")

                                <script type="text/javascript">

                                    function toggle_div(id) {

                                        var div = document.getElementById(id);

                                        if (div.style.display == 'block') {

                                            div.style.display = 'none';

                                        } else {

                                            div.style.display = 'block';
                                        }
                                    }

                                </script>

                                <div class="btn-group" style="float:right;">
                                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-cog"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>

                                    <ul class="dropdown-menu">
                                        <li><a href=<?php echo("'deleteEvent".$row['id_event']."'") ?> ><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span> Delete</a></li>
                                    </ul>

                                </div>

                                    @endif


                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="media">
                            <div class="media-left">

                                <a href="#">
                                    <a class="media-object" id="imageevent"> <?php echo("<img class=\"imgEvent\" src=" . $row['event_picture_url'] . "//>"); ?>
                                    </a>
                                </a>
                            </div>

                            <div class="media-body">
                                <h4 class="media-heading"> <?php echo $row['event_date'] ?></h4>
                                <?php echo $row['event_text'] ?>
                                <div class="clearfix"></div>
                                <div class="boutton">
                                    {!! Form::open(['url' => 'registration']) !!}
                                    <input type="hidden" name="id_event" value="{{$row['id_event']}}">
                                    {!! Form::submit('S\'inscrire', ['class' => 'btn btn-info pull-left']) !!}

                                    {!! Form::close() !!}

                                </div>

                                </br>
                                <button>

                                    <span class="button" onclick=<?php echo("\"toggle_div('file".$row['id_event']."')\"")?> >Ajouter une image</span>
                                </button>
                            </div>
                        </div></div></div>



                                <div class="divAddImg" id=<?php echo("'file".$row['id_event']."'") ?> >
                                {!! Form::open(['url' => 'addPicture', 'files' => true]) !!}

                                <div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">
                                    <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
                                    <input type="hidden" name="id_event" value=<?php echo($row['id_event'])?>>
                                    <input type="hidden" name="id_user" value="{{$user['id']}}">
                                    {!! Form::file('image', null, ['class' => 'form-control']) !!}
                                    {!! $errors->first('description', '<small class="help-block">:message</small>') !!}
                                </div>

                                {!! Form::submit('Valider', ['class' => 'btn btn-info pull-left']) !!}

                                {!! Form::close() !!}
                                </div>
            </div>
            </div>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

        @endauth


    @guest
@section('contenu')

    <h2>Merci de vous connecter ou de vous inscrire pour acceder à cette page</h2>

@endsection
    <div id="pouralign">
        <ul class="log">
            <li id="connexion"> <a href="/"> Connexion / Inscription </a> </li>
        </ul>


    </div>




    @endguest

<style>
    .divAddImg{
        display : none;
    }

</style>


@endsection