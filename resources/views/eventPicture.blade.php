@extends('template')


<link rel="stylesheet" href="/assets/css/styleEvent.css">


@section('sousimage')
    <p id="result"></p>
    <script type="text/javascript">
        function toggle_div(id) {

            var div = document.getElementById(id);

            if(div.style.display == 'block') {

                div.style.display = 'none';

            } else {

                div.style.display = 'block';

            }

        }

        function toggleAll(){
            var Elems = document.getElementsByClassName('divComs');
            for(var i=0; i < Elems.getLength(); i++ ){
                Elems[i].style.display = "none";

            }
            alert("Everything Toggled");
        }



    </script>


    <div class="allDivs">
        <div class="space"></div>
        <h1>{{"Evénement n°".$n}}</h1>
        <div class="space"></div>

        @foreach($pictures as $picture)
            <div class="container">
                <div class="picture">
                    <img src="<?php echo $picture['picture_url']?>">
                    <p><?php echo("Likes : ".\App\Http\Controllers\eventPictureController::getLikes($picture['id_picture'])) ?></p>
                    <div class="Like">
                        {!! Form::open(['url' => 'postLike']) !!}
                        <input type="hidden" name='id_user' value='<?php echo($user['id'])?>' >
                        <input type="hidden" name='id_picture' value=<?php echo("'".$picture['id_picture']."'")?> >
                        <input type="hidden" name='id_event' value=<?php echo("'".$picture['id_event']."'")?> >


                        {!! Form::submit('Like', null, ['id' => 'submit']) !!}
                        {!! Form::close() !!}

                    </div>



                    <span class="button" onclick=<?php echo("\"toggle_div('com".$picture['id_picture']."')\"")?> >Commenter</span>
                    <div class="divComs" id=<?php echo("\"com".$picture['id_picture']."\"")?>  >
                        {!! Form::open(['url' => 'postComment']) !!}
                        {!! Form::textarea('comment_text', null,['placeholder' => 'Commentez ici']) !!}


                        <input type="hidden" name='id_user' value=<?php echo("'".$user['id']."'")?> >
                        <input type="hidden" name='id_picture' value=<?php echo("'".$picture['id_picture']."'")?> >
                        <input type="hidden" name='id_event' value=<?php echo("'".$picture['id_event']."'")?> >


                        {!! Form::submit('Commenter', null, ['id' => 'submit']) !!}
                        {!! Form::close() !!}






                    </div>
                    <br/>
                    <span class="button" onclick=<?php echo("\"toggle_div('picture".$picture['id_picture']."')\"")?> >Afficher/Masquer les commentaires</span>



                    <?php $comments = \App\Http\Controllers\eventPictureController::getComments($picture['id_picture']); ?>
                    <div class="divComs" id=<?php echo("\"picture".$picture['id_picture']."\"")?>  >
                        @foreach($comments as $comment)
                            <h1>Commentaire numéro <?php echo $comment['id_comment'] ?></h1>
                            <p><?php echo $comment['comment_text'] ?></p>

            </div>




                    @endforeach

                </div>


            </div>


        @endforeach
        <div class="space"></div>

    </div>




    <style>
        .divComs{
            display: none;
        }
    </style>


@endsection

