
@extends('template')
<!--On "hérite" de la page template et on y ajoute du contenu -->
@section('titre')
    Contact
    @endsection
@section('head')
    <!--lien vers la feuille css utilisée -->
<link rel="stylesheet" href="assets/css/contact.css">
@endsection

@section('contenu')

    <br>
    <!--créaton d'une colonne d'affichage -->
    <div  class="col-sm-offset-3 col-sm-6">
        <div id="page">

            <!--Création du formulaire -->
        <div  id="info" class="panel panel-info">

            <!--titre du formulaire -->

            <div id="titre" class="panel-heading">Contact</div>

            <!-- corps du formulaire-->

            <div id="corps" class="panel-body">

                <!-- on veut pour un formulaire (Form),
                ouvrir (open) celui-ci, et qu'il pointe vers l'url "contact".-->

                {!! Form::open(['url' => 'contact']) !!}
                <div class="form-group {!! $errors->has('nom') ? 'has-error' : '' !!}">

                <!--On veut un élément de formulaire (Form) de type "email" avec le nom "email"
                    avec une valeur nulle et avec les attributs "class" et "placeholder" en précisant leur valeur.-->

                    {!! Form::text('nom', null, ['class' => 'form-control', 'placeholder' => 'Votre nom']) !!}

                <!--En cas de réception du formulaire suite à des erreurs on reçoit une variable$errors qui
                     contient un tableau avec comme clés les noms des contrôles et comme valeurs les textes identifiant
                     les erreurs.-->

                    {!! $errors->first('nom', '<small class="help-block">:message</small>') !!}
                </div>

                <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Votre email']) !!}
                    {!! $errors->first('email', '<small class="help-block">:message</small>') !!}
                </div>
                <div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">
                    {!! Form::textarea ('texte', null, ['class' => 'form-control', 'placeholder' => 'Votre message']) !!}
                    {!! $errors->first('texte', '<small class="help-block">:message</small>') !!}
                </div>
                <!--bouton d'envoi du formulaire-->
                {!! Form::submit('Envoyer !', ['class' => 'btn btn-info pull-right']) !!}

                {!! Form::close() !!}

            </div>
        </div>
    </div>
    </div>
@endsection