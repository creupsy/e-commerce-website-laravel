<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'AccueilController@index');
Route::get('boite-à-idée', 'boiteAIdeeController@index');
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache vidé</h1>';
});
Route::get('events', 'eventsController@index');
Route::get('contact', 'ContactController@getForm');
Route::post('contact', 'ContactController@postForm');
Route::get('idee', 'IdeeController@index');
Route::get('postEvent', 'postEventController@get');
Route::post('postEvent', 'postEventController@post');
Route::get('boutique', 'BoutiqueController@index');
Route::get('accueil', 'LoginController@get');
Route::post('accueil', 'LoginController@post');
Route::get('eventPicture{n}', 'eventPictureController@get')->where('n', '[0-9]+');
Route::post('postLike', 'likeController@post');
Route::get('details{n}', 'detailsController@index')->where('n', '[1-9]+');
Route::get('panier', 'PanierController@index');
Route::get('panier/addItem/{id}', 'PanierController@addItem');
Route::get('details{n}', 'detailsController@index')->where('n', '[0-9]+');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('postIdee', 'postIdeeController@get');
Route::post('postIdee', 'postIdeeController@post');
Route::get('IdeePicture{n}', 'IdeePictureController@get')->where('n', '[0-9]+');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('deleteEvent{n}', 'deleteEventController@get')->where('n', '[0-9]+');
Route::get('deleteIdea{n}', 'ideeController@delete')->where('n', '[0-9]+');
Route::post('vote', 'voteController@post');
Route::post('addPicture', 'addPictureController@post');
Route::post('postComment', 'addComment@post');
Route::get('moncompteParamètres', 'modifmoncompteController@get');
Route::get('moncompte', 'moncompteController@get');

Route::post('addToCart', 'commandeController@post');

Route::get('panier', 'commandeController@get');


Route::post('panier', 'commandeController@deleteCart');
Route::post('validationCommande', 'commandeController@validation');


Route::resource('utilisateurs', 'UserController');
Route::get('utilisateurs', 'utilisateurControlController@get');
//route::post('utilisateurs/{id}','userController@view');
Route::get('/utilisateur/{id}/edit', 'PostutilisateurControlController@edit');
Route::get('/utilisateur/{id}', 'PostutilisateurControlController@view');




Route::post('registration', 'eventRegistrationController@register');
Route::post('ideaToEvent', 'ideeToEvent@post');
Route::post('ideaToEventForm', 'ideeToEvent@get');

