<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_user extends Model
{
    protected $fillable = array('type_user');

    public function user_bde()
    {
        return $this->belongsTo(user_bde::class);

    }

}
