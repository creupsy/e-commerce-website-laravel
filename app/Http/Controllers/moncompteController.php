<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class moncompteController extends Controller
{
    public function get()
    {
        $user = Auth::user();
        return view('moncompte')->with("user", $user);
    }

}