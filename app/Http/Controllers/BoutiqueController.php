<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BoutiqueController extends Controller
{
    public function index()
    {
        //on récupere tous les produits de la table product et on les stockent dazns une variable
        $ordre = (isset ($_GET['order'])) ? $_GET['order'] : 'asc';
        $product = Product::all();
        //on fait de même pour les types de produits
        $product_type = \App\Product_type::all();

        $productOrder = Product::orderBy('product_price', $ordre)->get();

        $product = json_decode($product,true);
        //var_dump($product);

        $i = 0;
        $tab = array();
        foreach($product as $row) {
            $tab[$i] = $row['product_name'];
            $i++;
        }

        $user = Auth::user();
        //On renvoi à la vue la boutique ainsi qu'une liste de produit et de type de produits
        return view('boutique', array('name' => $tab,'productOrder'=>$productOrder,'product_type' => $product_type,'ordre'=>$ordre,'product'=>$product))->with("user", $user);
    }
}
