<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

class postIdeeController extends Controller
{
    public function get(){

        $user = Auth::user();
        return view('postIdee', ['user' => $user]);
    }

    public function post(){
        $ideas = new \App\idea;
        $ideas->idea_name = $_POST['titre'];
        $ideas->idea_text = $_POST['description'];
        $ideas->id_user = $_POST['user_id'];
        //$imageUploaded = $_POST['image'];

        if ($_FILES['image']['error'] > 0) $erreur = "Erreur lors du transfert";

        //if ($_FILES['image']['size'] > $maxsize) $erreur = "Le fichier est trop gros";


$extensions_valides = array( 'jpg' , 'jpeg' , 'png' );

$extension_upload = strtolower(  substr(  strrchr($_FILES['image']['name'], '.')  ,1)  );
if ( in_array($extension_upload,$extensions_valides) ){
    $nom = md5(uniqid(rand(), true));


    $chemin= "assets/img/events/{$nom}.{$extension_upload}";
    move_uploaded_file($_FILES['image']['tmp_name'],$chemin);
    // if ($resultat) echo "Transfert réussi";
    $ideas->idea_picture = $chemin;

}





        $ideas->save();
        $user = Auth::user();

        return view('ideaAccepted')->with('idea', $ideas)->with("user", $user);


    }
}
