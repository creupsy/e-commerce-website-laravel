<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ideeController extends Controller
{
    public static function index(){

        $ideas = \App\Idea::all();
        $user = Auth::user();
        return view('idee', array('ideas' => $ideas, "user" => $user));
    }

    public function delete($n){
        DB::table('votes')->delete();

        DB::table('ideas')->where('id_idea', '=',  $n)->delete();
        $vue = $this::index();
        return $vue;
    }

}



