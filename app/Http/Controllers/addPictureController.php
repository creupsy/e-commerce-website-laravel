<?php

namespace App\Http\Controllers;

class addPictureController extends Controller
{
   public function post(){
       $picture = new \App\Picture();
       $picture->id_user = $_POST['id_user'];
       $picture->id_event = $_POST['id_event'];

       //$imageUploaded = $_POST['image'];



       if ($_FILES['image']['error'] > 0) $erreur = "Erreur lors du transfert";

       //if ($_FILES['image']['size'] > $maxsize) $erreur = "Le fichier est trop gros";


       $extensions_valides = array( 'jpg' , 'jpeg' , 'png' );

       $extension_upload = strtolower(  substr(  strrchr($_FILES['image']['name'], '.')  ,1)  );
       if ( in_array($extension_upload,$extensions_valides) ){
           $nom = md5(uniqid(rand(), true));


           $chemin= "assets/img/events/{$nom}.{$extension_upload}";
           move_uploaded_file($_FILES['image']['tmp_name'],$chemin);
           // if ($resultat) echo "Transfert réussi";
           $picture->picture_url = $chemin;

       }





       $picture->save();


       $vue = eventsController::index();

       return $vue;





   }
}
