<?php

namespace App\Http\Controllers;


use App\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class commandeController extends Controller
{
    public function post(){
        if(isset($_POST['id_user']) && isset($_POST['id_product']) && isset($_POST['amount'])){
            $order = DB::table('orders')->where('id_user', '=', $_POST['id_user'])->where('id_state_order', '=', 1)->get();
            $cart = new Cart;

            if($order->count() == 0){

                $newOrder = new \App\Order;
                $newOrder->order_date = date('Y-m-d');
                $newOrder->id_state_order = 1;
                $newOrder->id_user = $_POST['id_user'];
                $newOrder->save();
                $order = DB::table('orders')->where('id_user', '=', $_POST['id_user'])->where('id_state_order', '=', 1)->get();

            }


            $order = json_decode($order);
            $order = commandeController::objectToArray($order[0]);
            $existCart = DB::table('carts')->where('id_order', '=', $order['id_order'])->where('id_product', '=', $_POST['id_product'])->get();

            if($existCart->count() > 0){
                $existCart = json_decode($existCart, true);
                $existCart = commandeController::objectToArray($existCart[0]);

                $cart->amount = intval($_POST['amount'], 10) + $existCart['amount'];
                DB::table('carts')->where('id_order', '=', $order['id_order'])->where('id_product', '=', $_POST['id_product'])->delete();
            }
            else{
                $cart->amount = $_POST['amount'];
            }

            $cart->id_product = $_POST['id_product'];
            $cart->id_order = $order['id_order'];


            $cart->save();



            return DetailsController::index($_POST['id_product'])->with('msg', 'Produit ajouté avec succès');
        }else{
            return DetailsController::index($_POST['id_product'])->with('msg', 'Produit non ajouté au panier');
        }
    }

    public static function objectToArray($d) {
        if (is_object($d)) {

            $d = get_object_vars($d);
        }

        if (is_array($d)) {

            return array_map(null, $d);
        }
        else {

            return $d;
        }
    }


    public function get(){
        $user = Auth::user();
        $order = DB::table('orders')->where('id_user', '=', $user['id'])->orderBy('id_state_order', 'asc')->get();
        $order = json_decode($order, true);
        return view('panier', ['order' => $order, 'user' => $user]);
    }

    public static function getProduct($id){
        return json_decode( DB::table('products')->where('id_product', '=', $id)->get(), true);
    }

    public static function getCart($id){
      return json_decode( DB::table('carts')->where('id_order', '=', $id)->get(), true);
    }

    public function deleteCart(){
        if(isset($_POST['id_order']) && isset($_POST['id_product'])){
            DB::table('carts')->where('id_product', '=', $_POST['id_product'])->where('id_order', '=', $_POST['id_order'])->delete();

        }
        return commandeController::get();
    }



    public function validation(){
        if(isset($_POST['id_order'])){
            DB::update('update orders set id_state_order = 2 where id_order = ?', [intval($_POST['id_order'], 10)]);

        }
        return commandeController::get();
    }

}
