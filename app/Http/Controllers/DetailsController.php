<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DetailsController extends Controller
{
    public static function index($n)
    {

        $product = DB::table('products')->where('id_product', '=',  $n)->get();
        $product = json_decode($product, true);

        $user = Auth::user();
        return view('details')->with(['product'=>$product, 'user' => $user]);
    }

}