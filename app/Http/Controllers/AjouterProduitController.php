<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

class AjouterProduitController extends Controller
{
    public function get(){
        return view('ajoutProd');
    }

    public function post(){
        $products = new \App\Product;
        $products->product_name= $_POST['nom_produit'];
        $products->product_description = $_POST['description'];
        $products->product_price = $_POST['price'];
        $products->product_count = 0;
        $products->id_type_product = $_POST['type_produit'];
        //$imageUploaded = $_POST['image'];



        if ($_FILES['image']['error'] > 0) $erreur = "Erreur lors du transfert";

        //if ($_FILES['image']['size'] > $maxsize) $erreur = "Le fichier est trop gros";


$extensions_valides = array( 'jpg' , 'jpeg' , 'png' );

$extension_upload = strtolower(  substr(  strrchr($_FILES['image']['name'], '.')  ,1)  );
if ( in_array($extension_upload,$extensions_valides) ){
    $nom = md5(uniqid(rand(), true));


    $chemin= "assets/img/boutiqueImg/{$nom}.{$extension_upload}";
    move_uploaded_file($_FILES['image']['tmp_name'],$chemin);
    // if ($resultat) echo "Transfert réussi";
    $products->product_picture_url = $chemin;

}





        $products->save();
        $user = Auth::user();

        return view('ajoutAccept')->with('products', $products)->with("user", $user);



    }
}

