<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ideePictureController extends Controller
{
     public static function get($n){

         $pictures = DB::table('pictures')->where('id_idea', '=',  $n)->get();
         $pictures = json_decode($pictures, true);

            $user = Auth::user();
            return view('idee')->with(['pictures' => $pictures, 'n' => $n ])->with("user", $user);
    }


    public static function getComments($id){
        $comments = DB::table('comments')->where('id_idea', '=', $id)->get();
        $comments = json_decode($comments, true);
        return $comments;
         }
}


