<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;



class utilisateurControlController extends Controller
{

    public function get()

    {
        //on récupere tous les produits de la table product et on les stockent dazns une variable
        $users = User::all();
        $user = Auth::user();
        //On renvoi à la vue la boutique ainsi qu'une liste de produit et de type de produits
        return view('utilisateurControl', compact('users'))->with("user", $user);
    }
}
