<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactRequest;

class ContactController extends Controller
{
    public function getForm()
    {
        //on renvoi à la vue le formulaire de contact
        return view('Contact');
    }

    public function postForm(ContactRequest $request)
    {
        //On soumet ensuite le fomulaire de contact rempli par l'utilisateur
        //Et on l'envoi à l'administateur du site
       /* Mail::send('email_contact', $request->all(), function($message)
        {
            $message->to('bdebordeaux33000@gmail.com')->subject('Contact');
        });*/
        //puis on renvoi à la vue une confirmation d'envoi
        return view('confirm_contact');
    }


}
