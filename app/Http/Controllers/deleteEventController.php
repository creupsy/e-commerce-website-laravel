<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class deleteEventController extends Controller
{

    public function get($n) {

        $user = Auth::user();
        DB::table('likes')->delete();
        DB::table('comments')->delete();
        DB::table('pictures')->where('id_event', '=',  $n)->delete();
        DB::table('events')->where('id_event', '=',  $n)->delete();
        return view('accueil')->with("user", $user);
    }
    //
}
