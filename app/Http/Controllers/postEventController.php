<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

class postEventController extends Controller
{
    public function get(){
        return view('postEvent');
    }

    public function post(){
        $event = new \App\Event;
        $event->event_title = $_POST['titre'];
        $event->event_date = $_POST['date'];
        $event->event_price = $_POST['price'];
        $event->event_text = $_POST['description'];
        //$imageUploaded = $_POST['image'];

        $event->event_status = 1;

        if ($_FILES['image']['error'] > 0) $erreur = "Erreur lors du transfert";

        //if ($_FILES['image']['size'] > $maxsize) $erreur = "Le fichier est trop gros";


$extensions_valides = array( 'jpg' , 'jpeg' , 'png' );

$extension_upload = strtolower(  substr(  strrchr($_FILES['image']['name'], '.')  ,1)  );
if ( in_array($extension_upload,$extensions_valides) ){
    $nom = md5(uniqid(rand(), true));


    $chemin= "assets/img/events/{$nom}.{$extension_upload}";
    move_uploaded_file($_FILES['image']['tmp_name'],$chemin);
    // if ($resultat) echo "Transfert réussi";
    $event->event_picture_url = $chemin;

}





        $event->save();
        $user = Auth::user();

        return view('postAccepted')->with('event', $event)->with("user", $user);



    }
}

