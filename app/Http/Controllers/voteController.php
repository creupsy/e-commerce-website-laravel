<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class voteController extends Controller
{
    public function post(){
        $user = Auth::user();
        $count = DB::table('votes')->where('id_user', $user['id'])->where('id_idea', $_POST['id_idea'])->count();
        if($count == 0){ \App\Vote::create(['id_user' => $user['id'], 'id_idea' =>  $_POST['id_idea']]);}

        $vue = ideeController::index();
        return $vue;
    }

public static function getVotes($id){
        $votes = DB::table('votes')->where('id_idea', '=', $id)->count();
        return $votes;
}
}
