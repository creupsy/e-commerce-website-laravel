<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

class eventsController extends Controller
{
    public static function index(){

        $events = \App\Event::all();
        $user = Auth::user();
        return view('events', array('events' => $events))->with("user", $user);

    }
}


