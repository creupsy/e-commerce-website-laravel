<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ideeToEvent extends Controller
{
    public function post(){
        $event = new \App\Event;
        $event->event_title = $_POST['titre'];
        $event->event_date = $_POST['date'];
        $event->event_price = $_POST['price'];
        $event->event_text = $_POST['description'];
        $event->event_picture_url = $_POST['image'];
        //$imageUploaded = $_POST['image'];

        $event->event_status = 1;



        //if ($_FILES['image']['size'] > $maxsize) $erreur = "Le fichier est trop gros";









        $event->save();
        DB::table('ideas')->where('id_idea', '=', $_POST['id_idea'])->delete();
        $user = Auth::user();

        return view('postAccepted')->with('event', $event)->with("user", $user);



    }

    public function get(){
            $idee= json_decode(DB::table('ideas')->where('id_idea', '=', $_POST['id_idea'])->get(), true);

            $user = Auth::user();
            return view('ideeToEvent')->with(['idea' => $idee, 'user' => $user]);
    }

}
