<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    public function index()
    {

        $users = User::orderBy('name', 'asc')->get();
//        $users = User::where('id', '>', 0)->simplePaginate(15);
//        $users->sortBy('name');
//        $users = $users->filter(function($user)
//        {
//            return $user->isAdmin();
//        });
//        dd($users);
        return view('utilisateurControl', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
//        $users = User::orderBy('name', 'desc')->get();
//        if ($_POST['filterParameter'] == "students"){
//            $users = User::orderBy('name', 'asc')->get();
//        }
//        return response()->json(array('users'=> $users), 200);
        if ($request->ajax()) {
            switch ($_POST['filterParameter']){
                case "students":
                    $users = User::where('id_type_user', '=', '1')->orderBy('name', 'asc')->get();
                    break;
                case "members":
                    $users = User::where('id_type_user', '=', '2')->orderBy('name', 'asc')->get();
                    break;
                case "employees":
                    $users = User::where('id_type_user', '=', '3')->orderBy('name', 'asc')->get();
                    break;
            }
            return view('listeUtilisateurs', ['users' => $users])->render();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd("cc");
        $users = User::where('id', '=', $id)->get();
//        $users = User::where('id', '>', 0)->simplePaginate(15);
//        $users->sortBy('name');
//        $users = $users->filter(function($user)
//        {
//            return $user->isAdmin();
//        });
//        $users[1] = $user;
//        dd($users);
        return view('utilisateurs', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'id_type_user' => 'required',
        ]);

        $user = User::find($id);
        $user->id_type_user = $request->input('id_type_user');
        $user->name = $request->input('name');

        $user->save();

        return redirect('utilisateurs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::find($id);
        $users->delete();
        return redirect ('/utilisateurs')->with('success', 'Suppression réussie');
    }
}

