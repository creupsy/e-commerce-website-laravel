<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class eventRegistrationController extends Controller
{



    public function register(){
        $user = Auth::user();

        if(isset($user) && isset($_POST['id_event'])){
            if(eventRegistrationController::checkIfRegistered($user['id'], $_POST['id_event'])){

                $registration = new \App\Register;
                $registration->id_user = $user['id'];
                $registration->id_event = $_POST['id_event'];
                $registration->save();
            }
        }
        $registre = DB::table('registers')->where('id_event', '=', $_POST['id_event'])->get();
        $number = $registre->count();
        $registre = json_decode($registre, true);
        return view('eventRegisteredPeople')->with(['number' => $number, 'registres' => $registre]);
    }





    public static function checkIfRegistered($id_event, $id_user){
        $count = DB::table('registers')->where('id_event', '=', $id_event)->where('id_user', '=', $id_user)->count();
        if ($count == 0){
            return true;
        }
        else{
            return false;
        }
    }

    public static function getUserMail($id){

            $user = json_decode(DB::table('user_bdes')->where('id', '=', $id)->get(), true);
            foreach($user as $use){
                $mail = $use['email'];
            }
            return $mail;

    }
}
