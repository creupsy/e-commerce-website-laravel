<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class eventPictureController extends Controller
{
     public static function get($n){

         $pictures = DB::table('pictures')->where('id_event', '=',  $n)->get();
         $pictures = json_decode($pictures, true);

         $user = Auth::user();
            return view('eventPicture')->with(['pictures' => $pictures, 'n' => $n ])->with("user", $user);
    }


    public static function getComments($id){
        $comments = DB::table('comments')->where('id_picture', '=', $id)->get();
        $comments = json_decode($comments, true);
        return $comments;
         }
         public static function getLikes($id){
             $likes = DB::table('likes')->where('id_picture', '=', $id)->count();

             return $likes;
         }

         public function postComment(){




         }
}