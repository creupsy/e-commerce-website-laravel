<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class modifmoncompteController extends Controller
{
    public function get()
    {
        $user = Auth::user();
        return view('modifmoncompte')->with("user", $user);
    }

}