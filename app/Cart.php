<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $timestamps = false;
    public function order(){
        return $this->hasOne(order::class);
    }
    public function user_bde(){
        return $this->hasOne(user_bde::class);
    }


}
