<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    protected $fillable = array('idea_text', 'idea_name','idea_picture','idea_date');
    public function user_bde()
    {
        return $this->hasOne(user_bde::class);
        return $this->belongsToMany(user_bde::class,'id_idea');

    }
    public function vote(){
        return $this->belongsTo(vote::class);
    }

}
