<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    public $timestamps = false;
    public function user_bde(){
        return $this->hasOne(user_bde::class);
    }
    public function event(){
        return $this->hasOne(event::class);
    }
}
