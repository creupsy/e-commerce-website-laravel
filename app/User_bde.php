<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_bde extends Model
{
    protected $fillable = array('name','lastname','password','email');

    public function type_user()
    {
        return $this->hasOne(type_user::class);
    }

    public function order()
    {
        return $this->belongsTo(order::class);
    }
    public function comments()
    {
        return $this->belongsTo(comments::class);
    }
    public function idea()
    {
        return $this->belongsTo(idea::class);
        return $this->belongsToMany(idea::class,'id');


    }

    public function picture()
    {
        return $this->belongsTo(picture::class);
        return $this->belongsToMany(picture::class,'id');

    }
    public function event()
    {
        return $this->belongsToMany(event::class,'id');
    }

    public function vote(){
        return $this->belongsTo(vote::class);
    }
    public function cart(){
        return $this->belongsTo(cart::class);
    }
    public function like(){
        return $this->belongsTo(like::class);
    }
    public function register(){
        return $this->belongsTo(register::class);
    }
}
