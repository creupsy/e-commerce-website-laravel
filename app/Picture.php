<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $fillable = array('picture_url');

    public function comment()
    {
        return $this->belongsTo(comment::class);
    }
    public function event()
    {
        return $this->hasOne(event::class);
    }

    public function user_bde()
    {
        return $this->hasOne(user_bde::class);
        return $this->belongsToMany(user_bde::class,'id_picture');

    }
    public function like(){
        return $this->belongsTo(like::class);
    }
}
