<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    public function user_bde(){
        return $this->hasOne(user_bde::class);
    }
    public function idea(){
        return $this->hasOne(idea::class);
    }

    protected $fillable = ['id_user', 'id_idea'];
    public $timestamps = false;
}
