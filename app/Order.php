<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = ['id_user', 'id_picture', 'order_date'];
    public $timestamps = false;
    public function user_bde()
    {
        return $this->hasOne(user_bde::class);
        return $this->belongsTo(user_bde::class);

    }
    public function product()
    {
        return $this->belongsToMany(product::class,'id_order');
    }
    public function order_state()
    {
        return $this->hasOne(order_state::class);


    }
    public function cart(){
        return $this->belongsTo(cart::class);
    }
}
