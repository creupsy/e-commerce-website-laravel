<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = array('event_title', 'event_price','event_date','event_text','event_picture_url','event_status');
    public function picture()
{
    return $this->belongsTo(picture::class);


}
    public function user_bde()
    {
        return $this->belongsToMany(user_bde::class,'id_event');


    }
    public function register(){
        return $this->belongsTo(register::class);
    }
}
