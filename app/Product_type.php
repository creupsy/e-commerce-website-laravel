<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_type extends Model
{
    protected $fillable = array('type_product');
    public function product()
    {
        return $this->belongsTo(product::class);
    }
}
