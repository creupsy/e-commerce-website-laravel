<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //Dans ce modèle ci on définit les différents champs de notre table product
    protected $fillable = array('product_name','product_description','product_price','product_picture_url','product_count');
//On définit les liens de la table product avec d'autres tables
    public function product_type()
    {
        //un produit ne possède qu'un type de produit
        return $this->hasOne(product_type::class);

    }
    public function order()
    {
        //un produit peut etre plusieurs fois commandé(ici on spécifie la clé étrangère associée)
        return $this->belongsToMany(order::class,'id_product');

    }
}
