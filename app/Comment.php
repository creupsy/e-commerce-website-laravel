<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = array('comment_text', 'comment_date', 'id_user', 'id_picture');
    public function user_bde()
    {
        return $this->hasOne(user_bde::class);

    }
    public function picture()
    {
        return $this->hasOne(picture::class);



    }

}
