<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_state extends Model
{
    protected $fillable = array('state_order_description');

    public function order()
    {
        return $this->hasMany(order::class);
        return $this->belongsTo(order::class);
    }
}
