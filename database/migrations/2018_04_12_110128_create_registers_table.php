<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->integer('id_user')->unsigned();
            $table->integer('id_event')->unsigned();
            $table->primary(['id_user', 'id_event']);
            $table->foreign('id_user')->references('id')->on('user_bdes');
            $table->foreign('id_event')->references('id_event')->on('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
