<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->integer('id_user')->unsigned();
            $table->integer('id_picture')->unsigned();
            $table->primary(['id_user', 'id_picture']);
            $table->foreign('id_user')->references('id')->on('user_bdes');
            $table->foreign('id_picture')->references('id_picture')->on('pictures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
