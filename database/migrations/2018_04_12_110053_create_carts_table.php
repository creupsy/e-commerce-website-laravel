<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{


    protected $fillable = ['id_order', 'id_product', 'amount'];
    public $timestamps = false;

    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->integer('id_product')->unsigned();
            $table->integer('id_order')->unsigned();
            $table->integer('amount');
            $table->primary(['id_product', 'id_order']);
            $table->foreign('id_product')->references('id_product')->on('products');
            $table->foreign('id_order')->references('id_order')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
