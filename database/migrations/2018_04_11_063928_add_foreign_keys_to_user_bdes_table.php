<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserBdesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_bdes', function(Blueprint $table)
		{
			$table->foreign('id_type_user')->references('id_type_user')->on('type_users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_bdes', function(Blueprint $table)
		{
			$table->dropForeign('FK_users_bde_id_type_user');
		});
	}

}
