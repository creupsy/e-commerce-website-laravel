<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->integer('id_user')->unsigned();
            $table->integer('id_idea')->unsigned();
            $table->primary(['id_user', 'id_idea']);
            $table->foreign('id_user')->references('id')->on('user_bdes');
            $table->foreign('id_idea')->references('id_idea')->on('ideas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
