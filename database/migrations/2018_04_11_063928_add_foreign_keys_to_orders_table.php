<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->foreign('id_state_order')->references('id_state_order')->on('order_states')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_user')->references('id')->on('user_bdes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->dropForeign('FK_orders_id_state_order');
			$table->dropForeign('FK_orders_id_user');
		});
	}

}
