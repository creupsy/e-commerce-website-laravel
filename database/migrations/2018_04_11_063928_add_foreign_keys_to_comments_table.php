<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCommentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->foreign('id_picture')
                ->references('id_picture')->on('pictures')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('id_user')->references('id')->on('user_bdes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropForeign('FK_comments_id_picture');
            $table->dropForeign('FK_comments_id_user');
        });
    }

}
