<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //création de la table product avec les différents champs associés
        Schema::create('products', function (Blueprint $table) {
            //spécification du moteur utilisé car certain moteurs ne prennet pas en compte les clés étrangères
            $table->engine = 'InnoDB';
            $table->increments('id_product');
            $table->string('product_name');
            $table->text('product_description');
            $table->integer('product_price');
            $table->string('product_picture_url');
            $table->integer('product_count');
            //spécification des clés étrangères
            $table->integer('id_type_product')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}