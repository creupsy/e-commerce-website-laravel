<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBdesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()

{
Schema::create('user_bdes', function (Blueprint $table) {
    $table->engine = 'InnoDB';
    $table->increments('id');
    $table->string('name');
    $table->string('lastname')->nullable();
    $table->string('password');
    $table->string('email');
    $table->integer('id_type_user')->unsigned()->default(2);
    $table->rememberToken();
    $table->timestamps();
});
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bdes');
    }
}
