<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductsTypesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(TypeUsersTableSeeders::class);
        $this->call(StatutOrderSeeder::class);

    }
}
