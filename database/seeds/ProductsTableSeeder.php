<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //insertion d'enregitrements pour la bdd
        DB::table('products')->insert(array(
            array(
                'product_name' => 'T-shirt BDE',
                'product_description' => 'voici le premier modèle de t-shirt 100% coton',
                'product_price' => '25',
                'product_picture_url'=>'assets\img\boutiqueImg\tshirt.jpg',
                'product_count' => '1',
                'id_type_product' => '1',
            ),
            array(
                'product_name' => 'Bracelet design',
                'product_description' => 'bracelet homme/femme',
                'product_price' => '12',
                'product_picture_url'=>'assets\img\boutiqueImg\bracelet.jpg',
                'product_count' => '1',
                'id_type_product' => '2',
            ),
            array(
                'product_name' => 'sacoche ordinateur',
                'product_description' => 'prevu pour des ordinateurs dont la taille est comprise entre 12 et 15 pouces',
                'product_price' => '18',
                'product_picture_url'=>'assets\img\boutiqueImg\sacoche.jpg',
                'product_count' => '1',
                'id_type_product' => '2',

            ),
            array(
                'product_name' => 'mascotte bde',
                'product_description' => 'diponible en différentes tailles s,m,l,xl',
                'product_price' => '30',
                'product_picture_url'=>'assets\img\boutiqueImg\costume.jpg',
                'product_count' => '1',
                'id_type_product' => '1',

            ),
            array(
                'product_name' => 'porte cle',
                'product_description' => 'accesoire indispensable pour y accrocher ses cles',
                'product_price' => '5',
                'product_picture_url'=>'assets\img\boutiqueImg\porte_cle.jpg',
                'product_count' => '1',
                'id_type_product' => '1',

            ),
            array(
                'product_name' => 'sweat bde',
                'product_description' => 'sweat disponible en differents coloris',
                'product_price' => '30',
                'product_picture_url'=>'assets\img\boutiqueImg\sweat.png',
                'product_count' => '1',
                'id_type_product' => '1',

            ),


        ));
    }

}
