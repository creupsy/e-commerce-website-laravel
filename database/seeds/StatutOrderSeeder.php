<?php

use Illuminate\Database\Seeder;

class StatutOrderSeeder extends Seeder
{

    public function run()
    {
        DB::table('order_states')->insert(array(
            array(
                'id_state_order' => '1',
                'state_order_description' => 'En cours',
            ),
            array(
                'id_state_order' => '2',
                'state_order_description' => 'Validé',
            ),

        ));
    }

}
