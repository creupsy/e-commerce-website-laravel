<?php

use Illuminate\Database\Seeder;

class ProductsTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_types')->insert(array(
            array(
                'type_product' => 'vetement',
            ),
            array(
                'type_product' => 'accesoire',
            ),
            array(
                'type_product' => 'autre',
            ),

        ));


    }
}
